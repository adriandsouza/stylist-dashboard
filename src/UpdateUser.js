import React, { Component } from 'react';
import { Layout, Avatar, Button, Input } from 'antd';
import Title from 'antd/lib/typography/Title';
import { UserOutlined} from '@ant-design/icons'
import './UpdateUser.css';

const { Header} = Layout;
class UpdateUser extends Component {
    
   
    constructor()
    {   
        super();
        this.state={
            id:"",
            name: "",
            age: "",
            address: "",
            action: ""
          }
    }
    componentDidMount()
    { 
        
        fetch('http://localhost:3000/users/'+this.props.match.params.id).then((response) => {
            response.json().then((result) => {
                console.warn(result)
                 this.setState({ 
                     name:result.name,
                     age:result.age,
                     id:result.id,
                     action:result.action,
                     address:result.address,
                     

                  })
            })
        })
    }
    update()
    {
        fetch('http://localhost:3000/users/'+this.state.id, {
            method: "PUT",
            headers:{
                'Content-Type':'application/json'
            },
            body: JSON.stringify(this.state)
        }).then((result)=>{
            result.json().then((resp)=>{
                this.props.history.push('/home')
                alert("User data updated")
            })
        })
    }
render() {
        
        
return (
            
<div className="updateUser">
    <Layout>
      <Header style={{ background:"black" }}>
        <Title style={{ color:"white" }}> illusion Color Salon Spa
        <Avatar style={{float:"right", margin:"15px" }} icon={<UserOutlined />} />      
        </Title>
      </Header>
      </Layout>
      <div >
        <Title>User Update</Title>
                
            <Input className="updateUserInput"onChange={(event) => { this.setState({ id: event.target.value }) }}
                        placeholder="Index" value={this.state.id} /> <br /><br />
            <Input className="updateUserInput"onChange={(event) => { this.setState({ name: event.target.value }) }}
                        placeholder="Name" value={this.state.name} /> <br /><br />
            <Input className="updateUserInput"onChange={(event) => { this.setState({ age: event.target.value }) }}
                        placeholder="Age" value={this.state.age} /> <br /><br />
            <Input className="updateUserInput" onChange={(event) => { this.setState({address : event.target.value }) }}
                        placeholder="Address" value={this.state.address} /> <br /><br />
            <select className="updateUserInput" onChange={(event) => { this.setState({action : event.target.value }) }}
                        placeholder="Action" value={this.state.action} > <br /><br />
                    <option>Styling</option>
                    <option>Done</option>

            </select><br/><br/>
            <Button className="updateButton" onClick={() => { this.update() }}>Update User</Button>
      </div>
                
</div>
        );
    }
}

export default UpdateUser;