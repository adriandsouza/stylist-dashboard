import React from 'react'

function DashboardAbout() {
    return (
        <div>
            <p>
            Whether you wear your hair long or short, light or dark, straight or curly does not matter if the style suits your appearance and personality. It sometimes may take courage and conviction to follow your own taste but expressing your individuality is well worth it.

Short Hair: Short haircuts come in many variations from boy cut to pixie and bob. You may have to try a few haircuts to find the right short haircut for you.
Long Hair: There are styling variations galore for long hair. Long hair may be cut to even length or may be layered.
            </p>
            <img alt="Hair Styles"src="https://img-static.popxo.com/2016/12/1-different-hairstyles.jpg"></img>
            <img alt="Hair Styles"src="https://img-static.popxo.com/2016/12/2-different-hairstyles.jpg"></img>
            <img alt="Hair Styles"src="https://img-static.popxo.com/2016/12/3-different-hairstyles.jpg"></img>
            <img alt="Hair Styles"src="https://img-static.popxo.com/2016/12/4-different-hairstyles.jpg"></img>
        </div>
    )
}

export default DashboardAbout
