import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Home from './Home';
import './App.css';
import Protected from './Protected'
import Login from './Login';
import Logout from './Logout';
import AboutUs from './AboutUs';
import UpdateUser from './UpdateUser';


function App() {

  return (
    <Router>
    <div className="App">
       <Switch>
       <Route path="/AboutUs"><AboutUs/></Route>
       <Route path="/logout" component={Logout}/>    
       <Protected exact path="/home" component={Home} />
       <Protected exact path="/update/:id" component={UpdateUser} />
       <Route path="/">   <Login/> </Route>
         
    
       </Switch>
    </div>

    </Router>
  );
}

export default App;
