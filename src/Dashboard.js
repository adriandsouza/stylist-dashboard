import React, { Component } from 'react'
import { Table } from 'react-bootstrap';
import './Dashboard.css';
import{Link} from 'react-router-dom';


class Dashboard extends Component {
  constructor() {
    super();
    this.state={
      id:"",
      name: "",
      age: "",
      address: "",
      operation: ""
    }
}
componentDidMount() {
   this.getData()
}
getData()
{
    fetch("http://localhost:3000/users").then((response) => {
        response.json().then((result) => {
            this.setState({ list: result })
        })
    })
}

 
  render() {
    return (
      <div>
        {
        this.state.list ?
      <Table className="dashboardTable">
      <thead>
       <tr style={{border:"1px solid black", height:"50px"}}>
          <th>id</th>
          <th>Name</th>
          <th>Age</th>
          <th>Location</th>
          <th>Action</th>
          <th>Status</th>
          
          </tr>
      </thead>
      <tbody>
          {
          this.state.list.map((item, i) =>
          <tr>
          <td>{item.id}</td>
          <td>{item.name}</td>
          <td>{item.age}</td>
          <td>{item.address}</td>
          <td>{item.action}</td>
          <td><Link to={"/update/"+item.id}>Update </Link></td>
                                                
          </tr>)}
      </tbody>
      </Table>
      
                            
      : <p>Please Wait...</p>
        }
      
      </div>
  )
  }
}

export default Dashboard;