import React from 'react';
import { Link, useHistory } from 'react-router-dom';
import { Layout, Avatar, Breadcrumb, Button } from 'antd';
import { UserOutlined } from '@ant-design/icons';
import Title from 'antd/lib/typography/Title';
import "./Home.css";
import "./Dashboard.css";
import DashboardAbout from './DashboardAbout';

const { Header, Sider, Content } = Layout;
function AboutUs() {  
const history = useHistory();

const routeChange = () =>{ 
    let path = '/logout'; 
    history.push(path);
  }

  return (
      
<div className="Home">
<Layout>
    <Header style={{ background:"black" }}>
        <Title style={{ color:"white" }}> illusion Color Salon Spa
        <Avatar style={{float:"right", margin:"15px" }} icon={<UserOutlined/>} />
        
        <Button onClick={routeChange} style={{float:"right", margin:"15px" }}>Logout</Button>
        
        </Title>
    </Header>
  <Layout>
      
  
  <Layout className="site-layout-background" style={{ padding: '0px ' }}>
    <Sider style={{ background:"black",height:"1025px" }} className="site-layout-background" width={200}>     
        <div className="dashboardSider">
        <Link style={{ color:"white" }}  to="/home">    
        Dashboard
        </Link>
        </div>
        
        <div className="dashboardSider">
        <Link style={{ color:"white" }} to="/AboutUs">    
        About Us
        </Link>
        </div>
    </Sider>
        
    <Content style={{ margin: '0 16px' }}>
        <Breadcrumb style={{ margin: '16px 0' }}>
              <Breadcrumb.Item>About Us</Breadcrumb.Item>
              
        </Breadcrumb>
          <div className="dashboard__content" style={{ padding: 20, minHeight: 360 }}>
           <DashboardAbout/>
          </div>

    </Content>
        
  </Layout>

  </Layout>
      
  </Layout>
  </div>
  )
}

export default AboutUs
